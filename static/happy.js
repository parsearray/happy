$(function(){
    var Place = Backbone.Model.extend({
        url: '/api/v1/places/',
        schema: {
            name:       'Text',
            start:      'DateTime',
            end:        'DateTime'
        }
    });

    var template = "<form class=\"pure-form pure-form-aligned\">\
        <fieldset>\
            <div class=\"pure-control-group\">\
                <label for=\"name\">Name</label>\
                <input id=\"name\" type=\"text\">\
            </div>\
            <div class=\"pure-control-group\">\
                <label for=\"start\">Start-Time</label>\
                <input id=\"start\" type=\"text\">\
            </div>\
            <div class=\"pure-control-group\">\
                <label for=\"end\">End-Time</label>\
                <input id=\"end\" type=\"text\">\
            </div>\
            <div class=\"pure-controls\">\
                <button id=\"submit_btn\" class=\"pure-button pure-button-primary\">Submit</button>\
            </div>\
        </fieldset>\
    </form>"

this.place = new Place();

var Form = Backbone.View.extend({
    el: '#submitForm',
    initialize: function () {
        this.render();
    },
    render: function () {
        var formTemplate = _.template(template);
        this.$el.html(formTemplate);
    },
    model: this.place,
    events: {
      'click button#submit_btn': 'submit'
    },
    submit: _.bind(function(e) {
        this.place.set("name", $('#name').val());
        this.place.set("happyHour", {"start": $('#start').val(), "end": $('#end').val()});
        this.place.save();
    }, this),    
});

var submitForm = new Form();

$('#start').timepicker();
$('#end').timepicker();
});