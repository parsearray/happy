package happy

import (
	"encoding/json"
	"fmt"
	"github.com/JustinBeckwith/go-yelp/yelp"
	"github.com/guregu/null"
	"io/ioutil"
	"net/http"
	"strconv"
)

// @returns yelp.AuthOptions
func getYelpConfig(configFile string) yelp.AuthOptions {
	var options yelp.AuthOptions

	data, err := ioutil.ReadFile(configFile)
	if err != nil {
		panic(err)
	}

	err = json.Unmarshal(data, &options)
	if err != nil {
		panic(err)
	}

	return (options)
}

// @returns []Place a struct
// @returns error
func GetDataForPlaces(searchMap map[string]string) ([]Place, error) {
	var radius float64

	yelpAuthOptions := getYelpConfig("./config.json")
	latitude, err := strconv.ParseFloat(searchMap["lat"], 64)
	longitude, err := strconv.ParseFloat(searchMap["long"], 64)
	if searchMap["radius"] != "" {
		radius, err = strconv.ParseFloat(searchMap["radius"], 64)
	} else {
		radius = 200.0
	}

	fmt.Println(radius)

	search_options := yelp.SearchOptions{
		GeneralOptions: &yelp.GeneralOptions{
			Term:         "bars",
			RadiusFilter: null.FloatFrom(radius),
		},
		LocationOptions: &yelp.LocationOptions{
			searchMap["zip"],
			&yelp.CoordinateOptions{
				Latitude:  null.FloatFrom(latitude),
				Longitude: null.FloatFrom(longitude),
			},
		},
	}

	client := yelp.New(&yelpAuthOptions, nil)
	results, err := client.DoSearch(search_options)
	if err != nil {
		panic(err)
	}

	//Parse get businesses back.
	parseClient := &http.Client{}
	req, _ := http.NewRequest("GET", "https://api.parse.com/1/classes/Places", nil)

	req.Header.Set("X-Parse-Application-Id", "2JruDxYRcq9V3Hjp0uBSCNyhmXvFQIinJZCE7qKd")
	req.Header.Set("X-Parse-REST-API-Key", "Ra5M4vN6LN5kjlFIsYB231d02LLuT64i1RicjSKv")
	req.Header.Set("Content-Type", "application/json")

	res, _ := parseClient.Do(req)

	defer res.Body.Close()

	decoder := json.NewDecoder(res.Body)
	var ParsePayload ParsePayload
	err = decoder.Decode(&ParsePayload)
	if err != nil {
		panic(err)
	}

	//We can refactor this if we need to add more information than just the happy hour from the Parse side.
	parseResults := make(map[string]HappyHour)

	for _, result := range ParsePayload.Results {
		//Should only have one place per result, but this is for safety purposes, due to Parse's JSON formatting.
		for _, places := range result.Places {
			parseResults[places.Name] = places.HappyHour
		}
	}

	/* For debugging purposes
	m, err1 := json.MarshalIndent(ParsePayload, "", "  ")
	if err1 != nil {
		panic(err1)
	}
	fmt.Println(string(m))
	*/

	places := make([]Place, len(results.Businesses))
	for i := 0; i < len(results.Businesses); i++ {
		places[i] = Place{Name: results.Businesses[i].Name, YelpLink: "", HappyHour: HappyHour{Start: parseResults[results.Businesses[i].Name].Start, End: parseResults[results.Businesses[i].Name].End}}
	}

	return places, nil
}

/*
func getPlacesById(searchMap map[string]string) {
}
*/
