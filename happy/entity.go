package happy

type Payload struct {
	Place []Place `json:"places"`
}

type Place struct {
	Name      string    `json:"name"`
	YelpLink  string    `json:"yelpLink"`
	HappyHour HappyHour `json:"happyHour"`
}

type HappyHour struct {
	Start string `json:"start"`
	End   string `json:"end"`
}

type ParsePayload struct {
	Results []Result `json:"results"`
}

type Result struct {
	CreatedAt string  `json:"createdAt"`
	ObjectId  string  `json:"objectId"`
	Places    []Place `json:"places"`
	UpdatedAt string  `json:updatedAt"`
}
