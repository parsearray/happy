package main

import (
	"encoding/json"
	"fmt"
	"github.com/go-martini/martini"
	// looks in $GOPATH/src/happy/happy
	"bytes"
	"github.com/martini-contrib/render"
	"happy/happy"
	"net/http"
)

func main() {
	m := martini.Classic()
	m.Use(render.Renderer())
	m.Use(martini.Static("static"))

	m.Get("/", getIndexPage)
	m.Get("/api/v1/places", getPlaces)
	m.Get("/api/v1/places/:id", getPlacesById)
	m.Post("/api/v1/places", postPlaces)

	m.Get("/post", func(r render.Render) {
		r.HTML(200, "postform", "")
	})

	m.Run()
}

func getIndexPage(r render.Render) {
	r.HTML(200, "index", "")
}

func getPlaces(params martini.Params, req *http.Request) (int, string) {
	qs := req.URL.Query()
	requestParam := map[string]string{
		"lat":    qs.Get("lat"),
		"long":   qs.Get("long"),
		"zip":    qs.Get("zip"),
		"radius": qs.Get("radius")}
	fmt.Println(requestParam)

	response, err := happy.GetDataForPlaces(requestParam) //ForGetPlaces()
	if err != nil {
		return 500, string(err.Error())
	}
	p := happy.Payload{}
	p.Place = response

	json_response, err := json.MarshalIndent(p, "", "  ")
	if err != nil {
		panic(err)
	}
	return 200, string(json_response)
}

func getPlacesById(params martini.Params) (int, string) {
	requestParam := map[string]string{
		"id": params["id"]}

	fmt.Println(requestParam)

	response, err := getJsonResponse() //ForGetPlacesById()
	if err != nil {
		return 500, string(err.Error())
	}
	return 200, string(response)
}

func postPlaces(r *http.Request) (int, string) {
	//TODO: add error handling to ParseForm
	decoder := json.NewDecoder(r.Body)
	var Place happy.Place
	err := decoder.Decode(&Place)
	if err != nil {
		return 500, string(err.Error())
	}

	m, err := json.MarshalIndent(Place, "", "  ")
	if err != nil {
		return 500, string(err.Error())
	}
	fmt.Println(string(m))

	p := happy.Payload{}
	p.Place = []happy.Place{}
	p.Place = append(p.Place, Place)

	marshalledPayload, err := json.MarshalIndent(p, "", "  ")
	if err != nil {
		return 500, string(err.Error())
	}
	fmt.Println(string(marshalledPayload))

	var jsonMarshalledPayloadBytes = []byte(string(marshalledPayload))

	client := &http.Client{}
	req, _ := http.NewRequest("POST", "https://api.parse.com/1/classes/Places", bytes.NewBuffer(jsonMarshalledPayloadBytes))

	req.Header.Set("X-Parse-Application-Id", "2JruDxYRcq9V3Hjp0uBSCNyhmXvFQIinJZCE7qKd")
	req.Header.Set("X-Parse-REST-API-Key", "Ra5M4vN6LN5kjlFIsYB231d02LLuT64i1RicjSKv")
	req.Header.Set("Content-Type", "application/json")

	res, _ := client.Do(req)

	defer res.Body.Close()

	return 201, string(marshalledPayload)
}

func getJsonResponse() ([]byte, error) {

	place := happy.Place{"fruits", "vegetables", happy.HappyHour{"start", "end"}}
	p := happy.Payload{}
	p.Place = append(p.Place, place)

	return json.MarshalIndent(p, "", "  ")
}
