
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Happy.io REST API for happy hours">

    <title>Happy.io</title>

    <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/pure-min.css">
    <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/grids-responsive-min.css">
    <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.css">
    <link rel="stylesheet" href="index.css">

</head>
<body>
    <div class="header">
        <div class="home-menu pure-menu pure-menu-horizontal pure-menu-fixed">
            <a class="pure-menu-heading" href="">Happy</a>

            <ul class="pure-menu-list">
                <li class="pure-menu-item pure-menu-selected"><a href="#" class="pure-menu-link">Home</a></li>
                <li class="pure-menu-item"><a href="#" class="pure-menu-link">Docs</a></li>
                <li class="pure-menu-item"><a href="/post" class="pure-menu-link">Submit</a></li>
            </ul>
        </div>
    </div>

    <div class="splash-container">
        <div class="splash">
            <h1 class="splash-head">Happy.io</h1>
            <p class="splash-subhead">
                A REST-ful API for those who seek happy hours.
            </p>
            <p>
                <a href="#" class="pure-button pure-button-primary">Get Started</a>
            </p>
        </div>
    </div>

    <div class="content-wrapper">
        <div class="content">
            <h2 class="content-head is-center">Because no good story ever started with a salad.</h2>

            <div class="pure-g">
                <div class="l-box pure-u-1 pure-u-md-1-2 pure-u-lg-1-4">

                    <h3 class="content-subhead">
                        <i class="fa fa-rocket"></i>
                        Get started quickly
                    </h3>
                    <p>
                        There's not much to read.
                    </p>
                </div>
                <div class="l-box pure-u-1 pure-u-md-1-2 pure-u-lg-1-4">
                    <h3 class="content-subhead">
                        <i class="fa fa-street-view"></i>
                        Location-specific data
                    </h3>
                    <p>
                        Because that bar across the street is a lot easier to get to than the one in Amsterdam.
                    </p>
                </div>
                <div class="l-box pure-u-1 pure-u-md-1-2 pure-u-lg-1-4">
                    <h3 class="content-subhead">
                        <i class="fa fa-code-fork"></i>
                        Open-source
                    </h3>
                    <p>
                        <a href="https://bitbucket.org/parsearray/happy">Code.</a>
                        But if you see us on the street, drinks are on you.
                    </p>
                </div>
                <div class="l-box pure-u-1 pure-u-md-1-2 pure-u-lg-1-4">
                    <h3 class="content-subhead">
                        <i class="fa fa-car"></i>
                        Just don't drink and drive
                    </h3>
                    <p>
                        Not worth it.
                    </p>
                </div>
            </div>
        </div>

        <div class="ribbon l-box-lrg pure-g">
            <div class="l-box-lrg is-center pure-u-1 pure-u-md-1-2 pure-u-lg-2-5">
                <i class="fa fa-spinner fa-pulse fa-5x"></i>
            </div>
            <div class="pure-u-1 pure-u-md-1-2 pure-u-lg-3-5">

                <h2 class="content-head content-head-ribbon">Stop waiting, get stoked.</h2>

                <p>
                    Most of our happy hour information is submitted by our users.  Feel free to send submissions in via our API, or through our web form.  Now, get out there and have a good time.
                </p>
            </div>
        </div>

        <div class="content">
            <h2 class="content-head is-center">Questions</h2>

            <div class="pure-g">
                <div class="l-box-lrg pure-u-1 pure-u-md-2-5">
                    <blockquote>
                        <p>All that is gold does not glitter,</p>
                        <p>Not all those who wander are lost;</p>
                        <p>The old that is strong does not wither,</p>
                        <p>Deep roots are not reached by the frost.</p>
                        <footer>— J.R.R. Tolkien</footer>
                    </blockquote>
                </div>

                <div class="l-box-lrg pure-u-1 pure-u-md-3-5">
                    <h4>Can I use this for anything?</h4>
                    <ul>
                        <li>Yes. Anything.</li>
                    </ul>
                    <h4>What is Happy.io written in?</h4>
                    <ul>
                        <li>Go (Martini)</li>
                        <li>Javascript (Backbone)</li>
                        <li>Parse</li>
                    </ul>
                </div>
            </div>

        </div>

        <div class="footer l-box is-center">
            By Paul and Ray.
        </div>

    </div>

</body>
</html>
